import { IsNotEmpty } from 'class-validator';
import { Length, Matches } from 'class-validator/types/decorator/decorators';

export class CreateUserDto {
  @IsNotEmpty()
  @Length(4, 16)
  login: string;

  @IsNotEmpty()
  @Length(4, 16)
  name: string;

  @IsNotEmpty()
  @Matches('^(?=.*[A-Za-z])(?=.*d)(?=.*[@$!%*#?&])[A-Za-zd@$!%*#?&]{8,}$')
  password: string;
}
